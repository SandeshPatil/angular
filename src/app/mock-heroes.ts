import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Katsuki Bakugou', Quirk: 'Explosion' },
  { id: 12, name: 'Shoto Todoroki', Quirk: 'Fire and Ice' },
  { id: 13, name: 'Izuku Midoriya', Quirk: 'One for All' },
  { id: 14, name: 'Fumikage Tokoyami', Quirk: 'Dark Shadow' },
  { id: 15, name: 'Ochako Uraraka', Quirk: 'Zero Gravity' },
  { id: 16, name: 'Mina Ashido', Quirk: 'Acid Attack' },
  { id: 17, name: 'Momo Yaoyorozu', Quirk: 'Creation' },
  { id: 18, name: 'Ejiro Kirishima', Quirk: 'Hardening' },
  { id: 19, name: 'Denki Kaminari', Quirk: 'Electricity' },
  { id: 20, name: 'Tenya Iida', Quirk: 'Engine  ' }
];
